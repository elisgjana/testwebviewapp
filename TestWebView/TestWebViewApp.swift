//
//  TestWebViewApp.swift
//  TestWebView
//
//  Created by Elis Gjana on 24.10.22.
//

import SwiftUI

@main
struct TestWebViewApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
