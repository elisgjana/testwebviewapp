//
//  WebView.swift
//  TestWebView
//
//  Created by Elis Gjana on 24.10.22.
//

import SwiftUI
import WebKit
 
struct WebView: UIViewRepresentable {
  
  var url: URL
  
  func makeUIView(context: Context) -> WKWebView {
    let webView = WKWebView()
    webView.pageZoom = 2.0
    return webView
  }
  
  func updateUIView(_ webView: WKWebView, context: Context) {
    let request = URLRequest(url: url)
    webView.load(request)
  }
}
