//
//  ContentView.swift
//  TestWebView
//
//  Created by Elis Gjana on 24.10.22.
//

import SwiftUI
import WebKit

struct ContentView: View {
    var body: some View {
      WebView(url: URL(string: "https://www.youtube.com")!)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
